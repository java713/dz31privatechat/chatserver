package academy.prog;

import java.util.ArrayList;
import java.util.List;

public class JsonMessages {
    private final List<Message> list = new ArrayList<>();

    public JsonMessages(List<Message> sourceList, int fromIndex, String login) {
        for (int i = fromIndex; i < sourceList.size(); i++) {
            Message msg = sourceList.get(i);
            if (msg.getTo() == null )
                list.add(msg);
            else
                if (msg.getTo().equals(login))
                    list.add(msg);
        }
    }
}
